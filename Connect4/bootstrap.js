// Connect4 -- bootstrap.js
var app = this;

app.game_engine = require('./lib/engine');
app.player1 = require('./lib/abstract_player');
app.player2 = require('./lib/abstract_player');
app.netplay_active = false;
app.is_local_game  = false;

/*
 * Connection / disconnection events
 */

app.on('connected', function() {
    console.log('Server found.');
});

app.on('disconnected', function() {
    console.log('Server lost.');

    if (app.netplay_active) {
        // disconnection should not trigger end of game if playing local came
        app.player1.opponent_left();
    }
});

/*
 * Messages from backend
 */

var last_seq = false;

app.on('message', function(action, data) {

    /*
     * parse and strip transport layer.
     * ack received messages.
     */

    if (action === 'message') {
        //console.log('got message: ' + JSON.stringify(data) + ')');

        // send ack
        app.msg("ack", {"seq": data.seq});

        // drop duplicates
        if (data.seq === last_seq) {
            console.log('dropping duplicate message ' + JSON.stringify(data));
            return;
        }
        last_seq = data.seq;

        // strip transport layer
        action = data.action;
        data = data.data;
    }

    /*
     * message handling
     */
    if (app.netplay_active) {
        app.player2.handle_message(action, data);

    } else {
        console.log('got message from backend: ' + action + ', data: ' + JSON.stringify(data));
    }
});
