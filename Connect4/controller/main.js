var _ = require('common/util');
var TextView = require('ui').TextView;
var HLayout = require('ui').HLayout;
var Control = require('ui').Control;
var logo = null;
var MenuItem = HLayout.extend({
	initialize: function(label, id) {
		MenuItem.__super__.initialize.call(this);

		this.style({ width: 'fill-parent', 'background-color': '#666' });
		this.text = new TextView();
		this.text.label(label);
		this.text.style({ color: '#fff', border: '5 5 5 5' });

		this.top = new Control();
		this.top.style({ width: 'fill-parent', height: 1, 'background-color': '#ccc' });

		this.add(this.top);
		this.add(this.text);

		this.id = id;
	},

	setFocus: function(focus) {
		if (focus) {
			this.style({ 'background-color': '#888' });
		} else {
			this.style({ 'background-color': '#555' });
		}
	},

	select: function() {
		console.log("Selected " + this.id);

		app.netplay_active = false;
		app.is_local_game  = false;

		// AI game
		if(this.id === 'ai'){
		  console.log("Start AI game");
	      app.player2 = require('./lib/ai_player');        
	      app.pushView('game');
		  app.player1.reset_game();
	      app.game_engine.set_first_move(1);
		}

		// Local game
		if(this.id === 'local') {
	      console.log("Start Local game");
	      app.player2 = require('./lib/local_player');
		  app.is_local_game = true;
	      app.pushView('game');
		  app.player1.reset_game();
		  app.game_engine.set_first_move(1);
		}

		// Net game
		if(this.id === 'net'){
	      console.log("Start Net game");
	      app.player2 = require('./lib/net_player');
	      app.pushView('game');
		  app.player1.reset_game();
	      app.player2.search_for_opponent();		
		}
	}
});

_.extend(exports, {
	':load': function() {
		var scene = this.get('menu');
		scene.defineSpritesheet('bg', app.imageURL('Bg_0.png'),   32, 32);
		scene.defineSpritesheet('logo', app.imageURL('logo4.png'),   160, 96);		
		
		scene.setLayers(2);

		this.add(new MenuItem('Single player',        'ai'));
		this.add(new MenuItem('Multiplayer Off-line', 'local'));
		this.add(new MenuItem('Multiplayer On-line',  'net'));

		this.setFocusedItem(1);

		logo = scene.add({
					sprite: 'logo',
					x: width /2 - 80,
					y: height/2 - 80,
					width: 160,
					height: 96,
					layer: 1,
					frame: 0
				});
	},

	':resized': function(width, height) {
		console.log('View was resized to ' + width + 'x' + height);
		
		var scene = this.get('menu');
		scene.setLayerBackground(0, {
				sprite: 'bg',
				x: 0,
				y: 0,
				width: width,
				height: height,
				layer: 0,
				tile: true
			});
			
			
		scene.change(logo,{
					x: width /2 - 80,
					y: height/2 - 80

				});

	},

	':keydown': function(key) {
		console.log('Key down: '+ key);
	},

	':keyup': function(key) {
		console.log('Key up: ' + key);
	},

	focusedItem: -1,

	':keypress': function(key) {
		console.log('Key press: ' + key);

		if (key === 'up') {
			if (this.focusedItem > 1) {
				this.setFocusedItem(this.focusedItem - 1);
			}
		}

		if (key === 'down') {
			this.setFocusedItem(this.focusedItem + 1);
		}

		if (key === 'fire') {
			this.get(this.focusedItem).select();
		}
	},

	':active': function() {
		console.log('View is active');
	},

	':inactive': function() {
		console.log('View is inactive');
	},

	setFocusedItem: function(item) {
		if (item < 0) {
			item = 0;
		}
		if (item >= this.size()) {
			item = this.size() - 1;
		}
		this.get(this.focusedItem).setFocus(false);
		this.focusedItem = item;
		this.get(this.focusedItem).setFocus(true);

		this.scrollTo(this.focusedItem);
	}
});
