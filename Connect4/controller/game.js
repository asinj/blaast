var _ = require('common/util');
console.log('Game is loaded');

//TextView to show messages
var HLayout = require('ui').HLayout;
var TextView = require('ui').TextView;

// Layer names and values
var layers = {
	background: 0,
	tokens: 1,
	blink: 2,
	cursor: 3,
	board: 4,
	popup_bg: 5,
	text: 6,
	scoreRed: 7,
	scoreBlue:8,
	timer: 9,
	count: 10
};


// Board has 7 rows and 6 lines for tokens and an extra line = 6 for the cursor.
// Note: Cell (0,0) is the bottom left cell.

var board = {
	offsetx: 0,  // Drawing offset for the board
	offsety: 0,  // Drawing offset for the board
	lines: 7,
	columns: 7,
	width: 224,  // 224 = columns * sprite_width  = 7 * 32
	height: 224  // 224 =  lines   * sprite_height = 7 * 32
	map = null;	// Array of Sprites: The board grid (painted on the screen)
};

// Maps of elements (Board map, token map , blinking map... )
// TODO: CLEANUP

var tokenA = null;	// Array of Sprites: The position of the tokens on the Board
var blinkA = null;	// Array of Sprites: contains the blinking tokens
var tokensWin= null;    // The coordinates array received from Game Engine 
var popup_bg_counter = true;  // Used to know the position of the popup_bg according to the screen orientation


// Properties of tokens falling
var dropping = {
	falling: false,
	token: null,
	position: 0,  //actual position
	line: 0 //destiny
};

//Properties of tokens blinking
var blink = {
	haswon: false,
	visible: false,
	count: 0
};

// Properties of the cursor
var cursor = {
	column: 0,  // 0 ... 6
	frame: 0,    // Frame used
	handle: 0    // Reference to the cursor sprite
};

// Global properties of the screen
var properties = {
	width: 0,    // Screen width
	height: 0,   // Screen height
	bg_frame: 0  // Current background frame (not actually frames, but whatever)
};

// Properties of the messages on screen.
// When adding a message to the screen, if you want it to be temporary, set count to zero (message.count = 0;)
// count will increase 25 times/sec and when it reaches max the message disappears.
var message = {
	count: 40,
	max:   40
};

var score = {
	red: 0,
	blue: 0,
	textRed: null,
	textBlue: null
};

// TODO: CLEANUP
var turn = 0;		// Current turn. 0 = Undefined, 1 = player 1, 2 = player 2
var game_over = false;  
var opponent_left_flag = false;
var all_tokens = [];//new Array(0);


var timer = {
	text: null,
	time: 0,
	timeSet: 750,
	pic : null  // Sprite of the timer_bg
};

_.extend(exports, {
	
// -----------------------------------------------------------------------------------
//                         GENERAL PAINTING SCREEN METHODS
//                Methods that paint static things on the screen.
// -----------------------------------------------------------------------------------
	/*
	   Changes the background. 
	   Frames do not work with "tile: true", so we must use three different spritesheets.
	   Spritesheets are initializes in function load().
	*/
	change_bg: function(frame) {
		frame = frame % 3;
		properties.bg_frame = frame;	

		var sprite;
		if (frame === 0) {
			sprite = 'bg_0';
		} else if (frame === 1) {
			sprite = 'bg_1';
		} else {
			sprite = 'bg_2';
		}

		
		var scene = this.get('scene');
		
		scene.setLayerBackground(layer.background, {
				sprite: sprite,
				x: 0,
				y: 0,
				width: properties.width,
				height: properties.height,
				layer: layers.background,
				tile: true
			});
	},

	':load': function() {
		var self = this;
		console.log('load() executing.');
		var scene = this.get('scene');

		// Sets the player1
		app.player1 = this;

		// Set the layers
		scene.setLayers(layers.count);

		
		scene.defineSpritesheet('bg_0',   app.imageURL('Bg_0.png'),   32, 32);
		scene.defineSpritesheet('bg_1',   app.imageURL('Bg_1.png'),   32, 32);
		scene.defineSpritesheet('bg_2',   app.imageURL('Bg_2.png'),   32, 32);
		scene.defineSpritesheet('tokens', app.imageURL('Tokens.png'), 32, 32);
		scene.defineSpritesheet('board',  app.imageURL('Board.png'),  32, 32);
		scene.defineSpritesheet('popup_bg',  app.imageURL('popup.png'),  32, 32);
		scene.defineSpritesheet('timer',  app.imageURL('Timer2.png'),  28, 36);
		scene.defineSpritesheet('score',  app.imageURL('score.png'),  48, 32);
		
		
		this.once('resized', function(width, height) {
			if (board.map === null){
			properties.width  = width;
			properties.height = height;
			}
			// Updates the background
			this.change_bg(0);
			
			// Board has 7 rows and 6 lines, but we must position it considering that it has an
			// "extra" line for the cursor.
			board.offsetx = width/2  - board.width / 2;
			board.offsety = height/2 + board.height / 2 - 32;
			var x;
			var y;
			//Creating Board Array
			board.map = new Array(7);

			for (var i=0; i < 7; i++) {
				board.map[i] = new Array(6);
			}
			//Creating Token Array
			
			tokenA = new Array(7);

			for (i=0; i < 7; i++) {
				tokenA[i] = new Array(6);
			}
			

			for (x = 0; x < 7; x++) {
				for (y = 0; y < 6; y++) {
					// NOTE: line 0 is for the cursor
					board.map[x][y] = scene.add({
						sprite: 'board',
						x: board.offsetx + 32 * x,
						y: board.offsety - 32 * y,
						width: board.width,
						height: board.height - 32,
						layer: layers.board,
						frame: 0
					});
				}
			}
	

			// Creates Popup background
			
			for (x = 0; x < Math.max(properties.width,properties.height); x+=32) {
					
						scene.add({
							sprite: 'popup_bg',
							x: x,
							y: properties.height /2 -16,
							width: 32,
							height: 32,
							layer: layers.popup_bg,
							frame: 0
						});
					
			}

			// Adds the cursor
			cursor.column = 3;
	        cursor.handle = scene.add({
		        sprite: 'tokens',
		        x: board.offsetx + (cursor.column) * 32,
		        y: board.offsety - 192,
		        width: 32,
		        height: 32,
		        layer: layers.cursor,
		        frame: frame
	        });
			// Shows the score message
			if (score.textRed === null) {
				score.textRed = new TextView();
				score.textBlue = new TextView();
				score.textRed.style({
						border: 5,
						color: 'white',
						width: 'fill-parent',
						align: 'left'
					});
				score.textBlue.style({
						border: 5,
						color: 'white',
						width: 'fill-parent',
						align: 'left'
					});
				scene.setLayerControl(layers.scoreRed, score.textRed);
				scene.setLayerControl(layers.scoreBlue, score.textBlue);
				scene.translate(layers.scoreRed, 1, 9);
				scene.translate(layers.scoreBlue, 23, 9);
			}
			this.update_score();
			
			// Creates the score board
			var scorePic = scene.add({
							sprite: 'score',
							x: 2,
							y: 2,
							width: 48,
							height: 32,
							layer: layers.board,
							frame: 0
						});		
			
			
			//Timer load
			timer.text = new TextView();
			timer.text.label(timer.time/25);
			timer.text.style({
				border: 5,
				color: 'white',
				width: 'fill-parent',
				align: 'rigth'
			});
			
			timer.pic = scene.add({
							sprite: 'timer',
							x: properties.width - 31,
							y: properties.height - 39,
							width: 40,
							height: 52,
							layer: layers.board,
							frame: 0
						});		
				
			scene.setLayerControl(layers.timer, timer.text);
			scene.translate(layers.timer, properties.width - 31, properties.height - 31);
			
			
		});
		
		//Animation loop for drop token
		this.setAnimationLoop(this.animation_loop, 1000/25);
	
		
		
		console.log('load() executed.');
	},
	':resized': function(width, height) {
		var scene = this.get('scene');
		// Resize after load. We already have tokens on the board
		if (board.map !== null){
			properties.width  = width;
			properties.height = height;
			
			board.offsetx = width/2  - board.width / 2;
			board.offsety = height/2 + board.height / 2 - 32;

			//changing the position of the board
			for (var x = 0; x < 7; x++) {
					for (var y = 0; y < 6; y++) {
						scene.change(board.map[x][y],{
							x: board.offsetx + 32 * x,
							y: board.offsety - 32 * y
						});
						//changing the position of the tokens
						if (tokenA[x][y] !== null){
						scene.change(tokenA[x][y],{
							x: board.offsetx + 32 * x,
							y: board.offsety - 32 * y
						});
						}
						
					}
				}
			for (x = 0; x < tokensWin.length; x++) {
						scene.change(blinkA[x],{
							x: board.offsetx + 32 * tokensWin[x].column,
							y: board.offsety - 32 * tokensWin[x].line
						});	
				}
			//changing the position of the arrow
			scene.change(cursor.handle,{
		        x: board.offsetx + (cursor.column) * 32,
		        y: board.offsety - 192
	        });
			//Switching position of popup_bg
			if (popup_bg_counter){
				scene.translate(layers.popup_bg, 0 ,  height/2 - width/2);
				popup_bg_counter= false;
			}	
			else{
				scene.translate(layers.popup_bg, 0 ,  0);
				popup_bg_counter= true;
			}
			//Switch position of score
			 scene.change(timer.pic,{
							x: properties.width - 31,
							y: properties.height - 39
						});		
			scene.translate(layers.timer, properties.width - 31, properties.height - 31);
			// Shows the score message
			this.update_score();
			
			this.change_bg(turn);
		}
		
	},


// -----------------------------------------------------------------------------------
//                         GAME INTERACTION PAINTING METHODS
//              Methods that paint stuff that changes during the game.
// -----------------------------------------------------------------------------------
	showCursor: function(column, frame) {
	    var scene = this.get('scene');
		    scene.change(cursor.handle, {
				sprite: 'tokens',
				x: board.offsetx + (column) * 32,
				frame: frame
				});
			scene.changeLayer( layers.cursor, {
				visible: true
				});
	},
	hideCursor: function() {
		var scene = this.get('scene');
		scene.changeLayer( layers.cursor, {
				visible: false
		});
	},

	hideMessage: function() {
		var scene = this.get('scene');
		scene.changeLayer(layers.popup_bg,{
					 visible: false
					});
		scene.changeLayer(layers.text,{
					 visible: false
					});
	},
	showMessage: function(msg) {

		var layout = new HLayout({
			style: {
				width: 'fill-parent',
				height: 'fill-parent',
				valign: 'middle',
				align: 'center',
				padding: 3
			}
		});		
				
		var text = new TextView();
		text.label(msg);
		text.style({
			border: 5,
			'font-weight': 'bold',
			'font-size': 'small',
			color: 'white',
			width: 'fill-parent',
			align: 'center'
		});
			
		layout.add(text);
				
		var scene = this.get('scene');
		scene.setLayerControl(layers.text, layout);

		scene.changeLayer(layers.popup_bg,{
					 visible: true
					});
		scene.changeLayer(layers.text,{
					 visible: true
					});
	},

	// Adds a token to a given place on the board. Returns a reference to the token.
	paint_token: function(line, column, frame, layer) {
		var scene = this.get('scene');
		var token = scene.add({
					sprite: 'tokens',
					x: board.offsetx + column * 32,
					y: board.offsety - line * 32,
					width: 32,
					height: 32,
					layer: layer,
					frame: frame
				});
		all_tokens.push(token);
		return token;
	},

	// Adds an opponent's token to the board
	// PARAMETERS - data: {player, column, line} as parameter
	add_token: function(data) {
		console.log('Token added!');
	
		tokenA[data.column][data.line] = this.paint_token(6, data.column, (data.player - 1) * 2 , layers.tokens);
		this.droptoken(data.line,tokenA[data.column][data.line]);
	
		console.log('game.add_token(): token added at (' + data.line + ',' + data.collumn + ')' );
	},

	droptoken:function(line,token){
		all_tokens.push(token);
		dropping.falling = true;
		dropping.token = token;
		dropping.position = board.offsety - 192;
		dropping.line = board.offsety - line * 32;
	},
	
	animation_loop: function(){
		// Cares about dropping tokens
		if (dropping.falling){
			this.hideCursor();

			var scene = this.get('scene');
			if (dropping.position < dropping.line){

				dropping.position += 16;
				scene.change(dropping.token,{
				y: dropping.position
				});	
			}
			else {
				dropping.falling = false;
				if (!game_over) {
					this.change_turn();
				}
			}	
		
		}
		// Cares about blinking
		this.animblink();

		// Cares about temporary messages on screen
		if (message.count < message.max) {
			message.count++;
			if (message.count === message.max) {
				this.hideMessage();
			}
		}
		
		//Timer for each turn
		
		if ((turn !== 0) && !dropping.falling && !game_over){
			console.log(timer.time);
			if (timer.time=== -1){
			cursor.column = Math.floor((Math.random()*7));
			timer.time = 0;
			}
			
			if (timer.time %25 ===0){
				timer.text.label(timer.time/25);
			}
			
			if (timer.time === 0){
				timer.time = -1;				
				this.emit('keypress','fire');

			}else{
			timer.time -= 1;
			}
		}
		
	},
	
	animblink: function(){
		if (blink.haswon && !dropping.falling ) {
			if (blink.count === 16){
				var scene = this.get('scene');
				blink.visible = !blink.visible;
				scene.changeLayer(layers.blink,{
				 visible: blink.visible
				});
				blink.count = 0;
			}
			else{
				blink.count += 1 ;
			}
		}
	},

	update_score: function() {
		if (score.red < 10){
			score.textRed.label(' ' + score.red);
		}else{
			score.textRed.label(score.red);
			}
		if (score.blue < 10){	
			score.textBlue.label(' ' + score.blue);
		}else{
			score.textBlue.label(score.blue);
		}
	},

// -----------------------------------------------------------------------------------
//                                 USER INPUT METHODS
//                            Dealing with the real world.
// -----------------------------------------------------------------------------------

	alert_invalid_move: function() {
		this.showMessage('Invalid move!');
		message.count = 0;
		console.log('User tried to make an invalid move!');
	},	
	
	':back': function() {
		this.exit_game();
	},

	':keypress': function(key) {

		if (opponent_left_flag === true) {
			this.exit_game();			
			return;
		}

		// Ignores commands while token is falling
		if (dropping.falling) {
			return;
		}
		if (game_over) {
			if (key === 'fire') {
				this.reset_game();
				if (app.netplay_active) {
					app.player2.restart_game(turn);
				}
			}
			return;
		}

		// Accepts or ignores user commands according to the turn.
		if (dropping.falling) {
			return;
		}

		if (key === 'left') {
            if (cursor.column > 0) {
				cursor.column = cursor.column - 1;
				if (turn === 1 || app.is_local_game){
					this.showCursor(cursor.column, (turn - 1)*2);
				}
            }
		}

		if (key === 'right') {
            if (cursor.column < 6) {
                cursor.column = cursor.column + 1;
                if (turn === 1 || app.is_local_game){
					this.showCursor(cursor.column, (turn - 1)*2);
				}
            }
		}

		if (key === 'fire' || key === 'down') {
			if(turn === 1 || app.is_local_game){
				var line = app.game_engine.try_to_add_token({player: turn, column: cursor.column, line: -1});
				console.log('try_to_add_token() returned line = ' + line);
				if( line > -1 ) {
					tokenA[cursor.column][line] = this.paint_token(6, cursor.column, (turn-1)*2, layers.tokens);
					this.droptoken(line,tokenA[cursor.column][line]);
				} else {
					if (timer.time !== -1){
					this.alert_invalid_move();
					}
				}		
			}			
		}
	},

// -----------------------------------------------------------------------------------
//                                GAME CONTROL METHODS
//                  Methods that control the game flow.
// -----------------------------------------------------------------------------------
	reset_game: function() {
		var scene = this.get('scene');

		// Removing tokens from the board
		_.forEach(all_tokens, function(value, index, array) {
			scene.remove(value);
		});
		all_tokens = [];//new Array(0);

		blink.haswon = false;
		game_over = false;
		app.game_engine.set_first_move(0);
		timer.time = timer.timeSet;
		timer.text.label(timer.time/25);
		
		// Turn turn turn
		//this.change_turn();

		this.hideMessage();
	},

	exit_game: function() {
		this.set_turn(0);	// Neutral turn
		cursor.column = 3;	// Neutral cursor
		app.game_engine.quit();
		opponent_left_flag = false;
		score.red = score.blue = 0;
		
		app.popView(); // pop the top view
		this.update_score();
	},

	// Toggles the turn variable.
	change_turn: function() {
	  timer.time = timer.timeSet;
	  if (turn === 1 ) { 
			app.player1.set_turn(2);
			app.player2.set_turn(2);
		} else if (turn === 2) { 
			app.player1.set_turn(1);
			app.player2.set_turn(1);
		}
	},

	// Sets the first player to have a turn.
	// Game Engine should set it in both players when game starts.
	// PARAMETERS - int player_id: player that makes the first move
	set_turn: function(player_id) {
		var scene = this.get('scene');
		turn = player_id;
	
		// Shows or hides the cursor.
		if (turn === 1) {
			this.showCursor(cursor.column, 0);
		} else if (turn === 2 && app.is_local_game) {
			this.showCursor(cursor.column, 2);
		} else { 
			this.hideCursor();
		}
	
		// Changes the color background
		this.change_bg(turn);
	
		if (!game_over) {
			this.hideMessage();
		}
		console.log('GUI: Turn set! Turn = ' + turn);
	},
	
	// Signals that the game ended
	// PARAMETERS - int winner_player: player that won
	// 4tokens: winner combination
	game_over: function(winner_player, tokens) {
		if(tokens === 0){
			console.log('Game over! Draw');
			game_over = true;
			this.showMessage('Game over !\r\nClick to play again');
		}
		else{
			console.log('Game over! Winner = ' + winner_player );
			
			var frame = 1; //winner local player, red blinks
			if (winner_player === 2 ){
				frame = 3; //winer remote, blue blinks 
			}
	
			// Draws the winner combination on an invisible layer
			var scene = this.get('scene');
			scene.changeLayer(layers.blink,{
					visible: false
				});
			var i;
			
			//saving information for turning issues
			tokensWin = tokens;
			blinkA = new Array(tokens.length);
			for (i = 0 ; i < tokens.length; i++){
				blinkA[i] =this.paint_token(tokens[i].line, tokens[i].column, frame, layers.blink);
			}
			// Starts Blink loop showing the winner combination.
			blink.haswon = true;
			blink.visible = false;
			
			game_over = true;
			if (winner_player === 1){
				score.red++;
				this.showMessage('Red player wins!\r\nClick to play again.\n');
				
			}else{
				score.blue++; 
				this.showMessage('Blue player wins!\r\nClick to play again.\n');
				
			}
			this.update_score();
			
			
		}
	},
	
	// Signals that the opponent left.
	// Can be called at any moment of the game.
	// PARAMETERS - none.
	opponent_left: function() {
		console.log('Opponent has left!');
		game_over = true;
		opponent_left_flag = true;
		this.showMessage('Game over !\r\nOpponent left the game.');
	}
	
});
