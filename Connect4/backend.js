// Connect4 -- backend.js


/*
 * testing
 */

// if enabled, drop every second ack
var test_drop_ack = false;
//var test_drop_ack = 1;

// define assert() to disable asserts
//function assert {}


/*
 * data
 */

var opponents = {};
var waiting_client = null;

/*
 * message sending
 */

var pending_messages = {};
var seq = 1;

function sendMsg(client, action, data, force) {
    if (pending_messages[client.id]) {
        console.log('sendMsg(): previous message for ' + client.id + ' still not delivered');
	if (!force) {
	        assert();
	        return false;
        }
    }

    pending_messages[client.id] = {'client': client, 'seq': seq, 'time': 0, 'action': action, 'data': data};

    client.msg('message', {'seq': seq, 'action': action, 'data': data});
    ++seq;
    return true;
}

function handleAck(client, data)
{
    if (pending_messages[client.id]) {

        if (!data || pending_messages[client.id].seq === data.seq) {
            if (data) {
                console.log('Client ' + client.id + ' acked message ' + data.seq);
            } else {
                console.log('Client ' + client.id + ' got message ' + pending_messages[client.id].seq);
            }

            delete pending_messages[client.id];

        } else if (data) {
            console.log('Client ' + client.id + ' acked unknown message ' + data.seq);
        }

    } else if (data) {
        console.log('Client ' + client.id + ' acked already acked message ' + data.seq);
    }
}

/*
 *
 */

function handleQuitGame(client) {

    handleAck(client, null);

    if (waiting_client === client) {
        waiting_client = null;
    }
    if (opponents[client.id]) {
        sendMsg(opponents[client.id], "opponent_disconnected", null, true);
        delete opponents[opponents[client.id].id];
        delete opponents[client.id];
    }
}

/*
 * resend timer
 */

// Every 5 seconds, check for pending non-acked messages
setInterval(function() {
    function empty_res(err,res){}

    for (var k in pending_messages) {
        var item = pending_messages[k];
        var client = item.client;

        item.time ++;
        if (item.time > 5) {
            // timeout, disconnect
            console.log('send: timeout, disconnect players');
            handleQuitGame(client);
            delete pending_messages[client.id];

        } else if (item.time > 1) {
            // resend
            console.log('resend ' + item.action + ' to ' + client.id);
            client.msg('message', {'seq': item.seq, 'action': item.action, 'data': item.data});
        }
    }
}, 5 * 1000);

/*
 * Client connection / disconnection events
 */

app.realtime(function(client, event) {
    if (event === 'CONNECTED') {
        console.log('Client ' + client.id + ' connected.');
    } else if (event === 'DISCONNECTED') {
        console.log('Client ' + client.id + ' disconnected.');
        handleQuitGame(client);
    }
});

/*
 * Client interface
 */

app.setClientPrototype({

    startGame: function(callback) {
        console.log('Client ' + this.id + ' called startGame()');

        handleAck(this, null);

        if (!waiting_client || waiting_client.id === this.id) {
            console.log('startGame(): client ' + this.id + ' waiting for opponent ...');
            waiting_client = this;
            delete opponents[this.id];
            callback(null, false);
        } else {
            var opponent = waiting_client;
            waiting_client = null;

            console.log('startGame(): client ' + this.id + ' found opponent ' + opponent.id);

            opponents[this.id] = opponent;
            opponents[opponent.id] = this;

            sendMsg(opponent, "start_game", {"opponent": this.id, "you_start": true});
            callback(null, opponent.id);
        }
    },

    addToken: function(data, callback) {
        console.log('Client ' + this.id + ' called addToken(' + JSON.stringify(data) + ')');

        handleAck(this, null);

        if (opponents[this.id]) {
            var ok = sendMsg(opponents[this.id], "token_added", data);
            callback(null, ok);
        } else {
            console.log('addToken(): no opponent !');
            callback(null, false);
        }
    },

    quitGame: function(callback) {
        console.log('Client ' + this.id + ' called quitGame()');
        handleQuitGame(this);
        callback(null, true);
    }
});

/*
 * Client messages
 */

app.message(function(client, action, data) {

    if (action === 'ack') {

        // test code: drop every second ack
        if (test_drop_ack) {
            ++test_drop_ack;
            if (test_drop_ack & 1) {
                console.log('TEST: dropped ack from client ' + client.user.id + ', msg ' + data.seq);
                return;
            }
        }
        // test code end

        handleAck(client, data);

    } else {
        console.log('message from client ' + client.user.id + ', action: ' +  action + ', data' + JSON.stringify(data));
    }
});
