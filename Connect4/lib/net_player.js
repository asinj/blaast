var net_player = require('./lib/abstract_player');

var cached_token = false;

/*
 * handle messages from backend
 */
exports.handle_message = function (action, data) {

	if (action === 'start_game') {
		console.log('got start_game: ' + JSON.stringify(data) + ')');

		app.game_engine.set_first_move(1);

		app.postNotification('opponentfound', 'Found opponent');

	} else if (action === 'token_added') {
		console.log('got token_added(' + JSON.stringify(data) + ')');

		data = data.data;
		data.player = 2;
		if (app.game_engine.try_to_add_token(data) < 0) {
			/* player has not yet decided to restart the game. Cache this move. */
			console.log('try_to_add_token() failed, caching token');
			cached_token = data;
		} else {
			cached_token = false;
		}

	} else if (action === 'opponent_disconnected') {
		console.log('got opponent_disconnected');

		app.game_engine.quit();

	} else {
		console.log('got unknown message from backend: ' + action + ', data: ' + JSON.stringify(data));
	}
};

/*
 * search for opponent
 */
exports.search_for_opponent = function() {
    app.netplay_active = true;

    app.request('startGame', function(err, result) {
        console.log('startGame() response was: ' + result);
        if (result) {
            console.log(' -- start playing --');

            app.game_engine.set_first_move(2);

        } else {
            console.log(' -- waiting for opponent --');
		app.player1.showMessage('Waiting for opponent');

        }
    });
};

/*
 * restart game with the same opponent
 */
exports.restart_game = function(turn) {
    console.log('restart_game()');
    if (turn === 2) {
        console.log('restart_game(): not my turn');
        if (cached_token) {
            /* opponent already did a move */
            console.log('restart_game(): found cached token');
            app.game_engine.try_to_add_token(cached_token);
            cached_token = false;
        } else {
            /* wait for first move or disconnect */
            console.log('restart_game(): waiting for opponent');
            app.player1.showMessage('Waiting for opponent');
        }
    }
};

/*
 * abstract player interface
 */

exports.add_token = function(data) {
	// Adds an opponent's token to the board
	// PARAMETERS - JSON data: {player, collumn} as parameter

        app.request('addToken', {"data": data}, function(err, result) {
		console.log('addToken() response was: ' + result);
	});
};

exports.set_turn = function(player_id) {
	// Sets player to have a turn.
	// Game Engine should set it in BOTH players when game starts and 
	// and every time, when turn is changed.
	// PARAMETERS - int player_id: player, whose turn it is.

	// not handled in net_player
};

exports.game_over = function(winner_player, tokens) {
	// Signals that the game ended
	// PARAMETERS - int winner_player: player that won
	//              JSON 4tokens: winner combination

	// not handled in net_player
};

exports.opponent_left = function() {
	// Signals that the opponent left.
	// Can be called at any moment of the game.
	// PARAMETERS - none.

	app.netplay_active = false;
        app.request('quitGame', function(err, result) {
		console.log('quitGame() response was: ' + result);
	});
};


