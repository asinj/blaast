/*
  Abstract Player class.

  Implementations of Player should override these functions.
  The Game Engine works with this abstract class ONLY!


  How to use this class:

  In another .js file, this abstract_class can be extended by:

	var abs_plr= require('./lib/abstract_player');
	abs_plr.opponent_left = function() {
		console.log('Opponent left! :(');
	};

	// Other functions...

*/

/* function assert() {} */

exports.add_token = function(data) {
	// Adds an opponent's token to the board
	// PARAMETERS - JSON data: {player, collumn, line} as parameter

	assert();
};

exports.set_turn = function(player_id) {
	// Sets player to have a turn.
	// Game Engine should set it in BOTH players when game starts and 
	// and every time, when turn is changed.
	// PARAMETERS - int player_id: player, whose turn it is.


	assert();
};

exports.game_over = function(winner_player, tokens) {
	// Signals that the game ended
	// PARAMETERS - int winner_player: player that won
	//              JSON tokens: winning combination
	// tokens is an array: {{column_0, line_0}, {column_1, line_1}, …, {column_n, line_n}}

	assert();
};

exports.opponent_left = function() {
	// Signals that the opponent left.
	// Can be called at any moment of the game.
	// PARAMETERS - none.

	assert();
};

