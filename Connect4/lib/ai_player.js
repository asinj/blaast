/*
Basic idea of AI:
AI calculates points for every columns and best column will be selected. If many columns
has same points, random column of them will be selected.

For every columns, AI calculates points so, that AI or opponent would add token in next free hole in columns. 
In addition, AI calculates points so, that AI or opponent would add token same column in next turn.

Points are calculated all 4 directions. For every direction there could be 1-4 sequential
token, which defines points.

Totally AI calculates sum of 28(2*2*7) different points and it is the points of column.
*/

var ai_player = require('./lib/abstract_player');

var first_move = 1;

//[AI       [This token[1,2,3,4], Next token[1,2,3,4]]
//[Opponent][This token[1,2,3,4], Next token[1,2,3,4]]
var points = [
 [ //AI
   [0, 5, 20, 1000], 
   [0, 1,  5,   30]
 ], 
 [ //Opponent
   [0,  5,  20, 100],
   [0, -5, -10, -50]
 ]
];

var column_is_full_points = -10000;

//mx, my
var directions = [
  [1,  1],
  [1,  0],
  [1, -1],
  [0, -1]
];

var myTurn = false;
var myId = 2;
var opId = 1;

/*
Returns random value from defined columns array.
*/
function random_column(columns)
{
  return columns[Math.floor((Math.random()*columns.length))];
}

/*
Returns next token row for defined column(0-5)

*/
function get_row(column)
{
  return app.game_engine.get_row(column);
}

/*
 Returns true, if player has token is defined hole. Otherwise return false.
*/
function is_token(player, col, row)
{
  return app.game_engine.is_token(player, col, row);
}

/*
This method calculates length of player sequential tokens.

Returns 1-4.

*/
function calc_len(player, col, row, mx, my)
{
  var len = 1;
  for(var i = 1; i <= 4; i++){
    if(is_token(player, col + mx * i, row + my * i) === true){
		  len = len + 1;
    }else{
		  break;
		}
  }
  for(i = 1; i <= 4; i++){
    if(is_token(player, col - mx * i, row - my * i) === true){
		  len = len + 1;
    }else{
		  break;
		}
  }	
	if(len > 4){
    
		return 4;
	}else{
    
	  return len;
	}
}

/*
This function calculates points for token.
*/
function calc_col_points(col, row)
{
  var total = 0;
	
  if(row >= 6){
    console.log("ai player: col " + col + " is full.[Row=" + row + "]");
    return column_is_full_points;
  }
  for(var i = 0; i < 4; i++)
  {
	  //AI, this token
    total += points[0][0][calc_len(myId, col, row,     directions[i][0], directions[i][1]) - 1];
	  //AI, next token
    total += points[0][1][calc_len(myId, col, row + 1, directions[i][0], directions[i][1]) - 1];
		//Opponent, this token
    total += points[1][0][calc_len(opId, col, row,     directions[i][0], directions[i][1]) - 1];
	  //Opponent, next token
    total += points[1][1][calc_len(opId, col, row + 1, directions[i][0], directions[i][1]) - 1];
  }
  return total;
}

/*
  This is main function for calculating best column to add token.
  Calculates points for each columns and select best one. If many columns
  has equals values, so returns random column of them.
  
  Returns index of calculated column(0-6).
*/
function calc_best_col()
{
  var best_points = -1000;
  var best_columns =[];

  for(var col = 0; col < 7; col++)
  {
    var row = get_row(col);
    var points = calc_col_points(col, row);
    console.log("Points for token [" + col + ", " + row + "] is " + points);
    if(points > best_points){
      best_points = points;
      best_columns = [col];
    }
    else if(points === best_points){
      best_columns.push(col);
    }
  }
  return random_column(best_columns);
}

function random_first_move() {
	return (Math.floor(Math.random()*3) + 2);
}

exports.add_token = function(data) {
	/*NOT NEEDED*/
};
exports.game_over = function(winner_player, tokens) {
	first_move = 1;
};
exports.opponent_left = function() {
	first_move = 1;
};

exports.set_turn = function(player_id) {  
  if(player_id === myId){
	if(first_move){
		app.game_engine.try_to_add_token({"column": random_first_move(), "player": myId});
		first_move = 0;
	}
	else{
		app.game_engine.try_to_add_token({"column": calc_best_col(), "player": myId});
	}
  }
};