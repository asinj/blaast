/*
  Game Engine class.

  In another .js file, the Game Engine can be accessed by:
	var engine = require('./lib/engine');

*/

// ----------------------------------------------------
//          FUNCTIONS CALLED BY THE PLAYERS
// ----------------------------------------------------
var GAME_OVER;
var TYPE;				//Type of game
var turn;
var won;
var last_movement = new Array(7);	//Actual state of game(columns)
var board = new Array(42);		//Actual state of game(board)
function token(player,type) {
	this.player = player;       // 0 = undefined, 1 = player1 , 2 = player2
	this.type = type;           // 0 = undefined, 1 = normal , 2 = arcade
}

/*
Returns last movement for column
*/
exports.get_row = function(column) {
  return last_movement[column];
};

/*
Returns true, if player has token in defined hole, otherwise false.
*/
exports.is_token = function(player, column, row) {
  if(column < 0 || column > 6 || row < 0 || row > 5){
    return false;
  }  
  return board[column + (row * 7)].player === player;
};

exports.init = function(type) {
	// Fills the board and last_movement array with initial values
	// PARAMETERS - none.
	TYPE = 1;   //Should be TYPE when arcade mode is developed.
	for (i=0;i<7;i++){
		last_movement[i] = 0;
	}
	for (i=0; i<42; i++){
		board[i] = null;
	}
	game_over = 0;
	won = 0;
};
exports.has_won = function(index){
	var result = {
		success : '',
		vector : null
	};
	var win = [];
	var counter = 1;
	var counterTotal = 0;
	var player = board[index].player;
	win[0] = index;
//In Horizontal RIGHT
	var i = index + 1;
	while(i%7 !== 0){
		if(board[i].player === player){
			win[counter] = i;
			counter++;
		}
		else{
			break;
		}
		i++;
	}
//In Horizontal LEFT
	i = index - 1;
	while(i%7 !== 6){
		if(board[i].player === player){
			win[counter] = i;
			counter++;
		}
		else{
			break;
		}
		i--;
	}
	if(counter < 4) {
		for(i=0; i<counter; i++){
			win.pop();
		}
		counter = 0;
		counterTotal = 1;
	}
	else{
		counterTotal = counter + counterTotal;
		counter = 0;
	}
//In Vertical
	i = index - 7;
	while(i >= 0){
		if(board[i].player === player){
			win[counter + counterTotal] = i;
			counter++;
		}
		else{
			break;
		}
		i = i - 7;
	}
	if(counter<3) { 
		for(i=0; i<counter; i++){
			win.pop();
		}
		counter = 0; 
	}
	else { 
		counterTotal = counterTotal + counter; 
		counter = 0;
	}
//In Diagonal 1 UP
	i = index + 6;
	while(i < 40 && index%7!==0){
		if(board[i].player === player){
			win[counter + counterTotal] = i;
			counter++;
		}
		else{
			break;
		}
		if(i%7 === 0){
			break;
		}
		i = i + 6;
	}
//In Diagonal 1 DOWN
	i = index - 6;
	while(i > 0 && index%7!==6){
		if(board[i].player === player){
			win[counter + counterTotal] = i;
			counter++;
		}
		else{
			break;
		}
		if(i%7 === 6){
			break;
		}
		i = i - 6;
	}
	if(counter < 3) { 
		for(i=0; i<counter; i++){
			win.pop();
		}
		counter = 0; 
	}
	else { 
		counterTotal = counterTotal + counter; 
		counter = 0;
	}
//In Diagonal 2 UP
	i = index + 8;
	while(i < 42 && index%7!==6){
		if(board[i].player === player){
			win[counter + counterTotal] = i;
			counter++;
		}
		else{
			break;
		}
		if(i%7 === 6){
			break;
		}
		i = i + 8;
	}
//In Diagonal 2 DOWN
	i = index - 8;
	while(i >= 0 && index%7!==0){
		if(board[i].player === player){
			win[counter + counterTotal] = i;
			counter++;
		}
		else{	
			break;
		}
		if(i%7 === 0){
			break;
		}
		i = i - 8;
	}
	if(counter < 3) { 
		for(i=0; i<counter; i++){
			win.pop();
		}
		counter = 0; 
	}
	else { 
		counterTotal = counterTotal + counter; 
	}
	if(counterTotal > 3){
		result.success = true;
		result.vector = win;
		return result;
	}
	else{
		result.success = false;
		result.vector = win;
		return result;
	}
};

exports.try_to_add_token = function(data) {
	// Adds a token to the board
	// PARAMETERS - JSON data: {player, collumn} as parameter
	// If valid, forwards the move to the other player. (via add_token(data))
	// It returns the line the token has fallen to, or -1 if invalid move

	console.log('try_to_add_token(' + JSON.stringify(data) +')');
	if(turn === data.player && !game_over){
		console.log('Turn: ' + turn);
		if(last_movement[data.column] < 6) {
			board[data.column + (last_movement[data.column]*7)] = new token(data.player, TYPE);		//Insert token in board
			data.line = last_movement[data.column];													//Put line in data for the other player
			last_movement[data.column]++;															//Update the top of columns
			if (data.player === 1) {																//Send the movement to the other player
				app.player2.add_token(data);
			} else {
				if (!app.is_local_game){
					app.player1.add_token(data);
				}
			}
			result = this.has_won(data.column + ((last_movement[data.column]-1)*7));					//Check if the player won.
			if (result.success) {
				for(i = 0; i<result.vector.length; i++){
					var coordinates = {line: Math.floor(result.vector[i]/7), column: result.vector[i]%7};
					result.vector[i] = coordinates;
				}
				app.player1.game_over(data.player, result.vector);
				app.player2.game_over(data.player, result.vector);
				game_over = 1;
				won = data.player;
			}
			else{
				var not_full = false;
				for(i=0; i<7; i++){																		//Is the board full? Game over.
					if(last_movement[i]<6){
						not_full = true;
					}
				}
				if(!not_full){
					app.player1.game_over(data.player, 0);
					app.player2.game_over(data.player, 0);
					game_over = 1;
				}
			}
			if(turn === 1){																			//Change turns.
				turn = 2;
			}
			else if(turn === 2){
				turn = 1;
			}
			return last_movement[data.column]-1;
		}
		else {
			console.log('Column full ! (last: '+last_movement[data.column]+')');
			return -1;
		}
	}
	else {
		console.log('It is not my turn.');
		return -1;
	}
};

exports.set_first_move = function(player_id) {
	// Sets the first player to have a turn.
	// Set it in BOTH players when game starts. (via set_turn())
	// PARAMETERS - int player_id: player that makes the first move
	// COMMENTS - When remacht, player_id === 0, game engine check who won last game, 
	//            and in the next game the looser will start

	console.log('set_first_move(' + player_id + ')');
	if(player_id === 0){
		// Start the looser.
		if(won === 1){
			turn = 2;
		}
		else if(won === 2){
			turn = 1;
		}
		else if(won === 0){
			console.log('Error: won = 0 and shouldnt');
		}
		console.log('turn(' + turn + ')');
	}
	else{
		turn = player_id;
	}
	this.init();
	app.player1.set_turn(turn);
	app.player2.set_turn(turn);
};

exports.quit = function() {
	// Signals that the player is leaving. Can be called at any moment of the game.
	// Also lets the other player know that their opponent is leaving the game. (via opponent_left())
	// PARAMETERS - none.

	console.log('Player quits game');

	app.player1.opponent_left();
	app.player2.opponent_left();
};

// ----------------------------------------------------
