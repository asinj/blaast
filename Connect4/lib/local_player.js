var local_player = require('./lib/abstract_player');

/*
	Local Player is just a dummy. It does nothing.
*/

exports.add_token = function(data) {};

exports.set_turn = function(player_id) {};

exports.game_over = function(winner_player, tokens) {};

exports.opponent_left = function() {};

